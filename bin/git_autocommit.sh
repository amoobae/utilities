#!/usr/bin/env bash
#First Argument is quote string of commit message
#Create a file with random content for testing
# base64 /dev/urandom | head -c 40 > file.txt

COMMIT_MESSAGE=${1:-"automated commit on server - no human involvements."}
DEBUG=false
GITSTATUS=`git status --porcelain`

if [ "$GITSTATUS" != "" ]
then
    git add -A
    git commit --quiet -m "${COMMIT_MESSAGE}"
    git pull --quiet
    git push --quiet
elif [ "$DEBUG" = true ]
then
    echo "Nothing to Commit"
fi