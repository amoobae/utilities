#!/usr/bin/env bash
DEBUG=false
UPDATE_PLUGINS=true

if [ "$DEBUG" = true ]; then echo "wp: updating core"; fi
wp core update --quiet
wp core update-db --quiet

if [ "$DEBUG" = true ]; then echo "wp: verifying core checksums"; fi
wp core verify-checksums --quiet

if [ "$UPDATE_PLUGINS" = true ]; then
    if [ "$DEBUG" = true ]; then echo "wp: updating plugins"; fi
    wp plugin update --all --quiet
fi
