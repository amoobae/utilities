#!/usr/bin/env bash
TZ=America/Los_Angeles
NOW=`date +'%Y-%m-%d-%H-%M-%S'`
DBNAME=`wp eval 'echo DB_NAME;'`
BACKUP_FILENAME="${DBNAME}-${NOW}.sql.gz"
YEAR=`date +'%Y'`
MONTH=`date +'%m'`
BACKUP_BUCKET=${AWS_BACKUP_BUCKET:-default_backup_bucket}
DEBUG=true
UPLOAD=false
UPLOAD_PATH="s3://${BACKUP_BUCKET}/wordpress/${DBNAME}/${YEAR}/${MONTH}/${BACKUP_FILENAME}"
WEEKLY_UPLOAD_PATH="s3://${BACKUP_BUCKET}/weekly/${DBNAME}/${YEAR}/${MONTH}/${BACKUP_FILENAME}"
DAY_OF_WEEK=`date +'%u'`

wp db export - | gzip -9 > $BACKUP_FILENAME

if [ "$DEBUG" = true ]; then
    echo "wp: exporting database ${DBNAME}"
    du -sh $BACKUP_FILENAME
fi

if [ "$UPLOAD" = true ]; then
    if [ "$DEBUG" = true ]; then echo "amazon s3: uploading to amazon s3 as $UPLOAD_PATH"; fi
    aws s3 cp $BACKUP_FILENAME $UPLOAD_PATH --quiet

    if [ "$DAY_OF_WEEK" = 6 ]; then
        aws s3 cp $BACKUP_FILENAME $WEEKLY_UPLOAD_PATH --quiet
    fi

    if [ "$DEBUG" = true ]; then echo "amazon s3: finished uploading, removing local file"; fi
fi
rm $BACKUP_FILENAME